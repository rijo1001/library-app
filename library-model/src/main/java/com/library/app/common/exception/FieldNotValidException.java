package com.library.app.common.exception;

public class FieldNotValidException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final String fieldName;

	public FieldNotValidException(final String fieldName, final String message) {
		super(message);
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	@Override
	public String toString() {
		return "FieldNotValidException [fieldName=" + fieldName + "]";
	}
}
