package com.library.app.commontests.category;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;

import com.library.app.category.model.Category;

public class CategoryForTestsRepository {

	@Ignore
	public static Category architecture() {
		return new Category("Architecture");
	}

	@Ignore
	public static Category cleanCode() {
		return new Category("Clean Code");
	}

	@Ignore
	public static Category java() {
		return new Category("Java");
	}

	@Ignore
	public static Category networks() {
		return new Category("Networks");
	}

	public static Category categoryWithId(final Category category, final Long id) {
		category.setId(id);
		return category;
	}

	public static List<Category> allCategories() {
		return Arrays.asList(java(), cleanCode(), architecture(), networks());
	}
}
